<?/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file downloads a song.
*/?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styling.css">
	<title>Donwload Song</title>
</head>
<body>
	<?php
	//Part of download code from http://www.php-mysql-tutorial.com/wikis/mysql-tutorials/uploading-files-to-mysql-database.aspx
	require_once('constants.php');
	require_once('db-utils.php');
	echo makeHeader();
	
	if(array_key_exists('id', $_GET))
	{
	// if id is set then get the file with the id from database

	//include 'library/config.php';
	//include 'library/opendb.php';

	$conn = connectToDb();
	$id    = $_GET['id'];
	$qu = "SELECT name, type, size, content " .
			 "FROM upload WHERE id = '$id'";

	$result = mysqli_query($conn, $qu) or die('Error, query failed');
	list($name, $type, $size, $content) = mysqli_fetch_array($result);
	

	header("Content-length: $size");
	header("Content-type: $type");
	header("Content-Disposition: attachment; filename=$name");
	echo $content;

	//include 'library/closedb.php';
	$mysqli_close($conn);
	exit;
	}

?>
</body>
</html>