<?php
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file is for util fuctions that involve database connection.
*/
error_reporting(E_ALL);
//ini_set('display_errors','On
require_once('utils.php');
/*
* Returns a connect to the database for other functions to use.
*/
function connectToDb()
{
	$password = 'security1#';
	$hostname = 'localhost';
	$username = 'proj2';
	$schemaToUse = 'proj2';
	try
	{
		$conn = mysqli_connect($hostname, $username, $password, $schemaToUse);
	}
	catch (Exception $e)
	{
		echo $e->getMessage();
	}	
	
	return $conn;
}

//Adds the given user to the database with the given password
function addUser($newName, $newPass)
{
		$conn = connectToDb();
		$newName = mysqli_real_escape_string($conn, $newName);
		$newPass = mysqli_real_escape_string($conn, $newPass);
		$qu = "INSERT INTO users VALUES ('$newName', '$newPass', '0')";
		$allRows = mysqli_query($conn, $qu);
		mysqli_close($conn);

}
//Returns a boolean if the given username exists in the database
function usernameExists($username)
{
	$exists = false;
	$conn = connectToDb();
	
	$qu = "SELECT username FROM users";
	$allRows = mysqli_query($conn, $qu);
	
	while($oneRow = mysqli_fetch_assoc($allRows))
	{
		if($oneRow['username'] == $username)
			$exists = true;
	}
	
	mysqli_close($conn);
	return $exists;
}
//Converts the SQL results of all the songs into an HTML table
function songsToTable()
{
	$SR = "<tr><td>"; //Start row
	$ER = "</td></tr>"; //End row
	$BC = "</td><td>"; //Between Col
	
	$conn = connectToDb();
	
	$qu = "SELECT * FROM songs";
	$allRows = mysqli_query($conn, $qu);
	if (!$allRows)  echo "query failed -- lost connection?";
	$stringSoFar = "<table border='1' cellpadding='5'><tr><th>Title</th><th>Artist</th><th>Album</th><th>Genre</th><th>Price</th></tr>";
	
	while($oneRow = mysqli_fetch_assoc($allRows))
	{
		$details = array('title' => htmlspecialchars($oneRow['title']), 'artist' => htmlspecialchars($oneRow['artist']));
		$stringSoFar = $stringSoFar . $SR . linkForGet($details, $oneRow['title']) . $BC . htmlspecialchars($oneRow['artist']) . $BC . htmlspecialchars($oneRow['album']) . $BC . htmlspecialchars($oneRow['genre']) . $BC . htmlspecialchars($oneRow['price']) . $ER;
		
	}
	
	$stringSoFar = $stringSoFar . "</table>";
	mysqli_close($conn);
	return $stringSoFar;
}

//gets all the songs the given user has purchased and creates an html out of it
function userSongsToTable($user){
	$SR = "<tr><td>"; //Start row
	$ER = "</td></tr>"; //End row
	$BC = "</td><td>"; //Between Col
	$OH = "<a href='download-song.php?id=";
	$CH = "</a>";
	$conn = connectToDb();
	
	$qu = "SELECT * FROM song_ownership where user = '$user'";
	$qu2 = "SELECT admin from users where username = '$user'";
	
	$allRows = mysqli_query($conn, $qu);
	$allRows2 = mysqli_query($conn, $qu2);
	if (!$allRows)  echo "query failed -- lost connection?";
	if (!$allRows2)  echo "query failed -- lost connection?";
	$oneRow2 = mysqli_fetch_assoc($allRows2);
	if ($oneRow2['admin'] == 1){
		echo "</br><form name='admin-button' action='upload.php'><input type='submit' value='Add song'></form>";
	}
	
	$stringSoFar = "<table border='1' cellpadding='5'><tr><th>Title/Download</th><th>Artist</th><th>Album</th><th>Price paid</th><th>Date purchased<br/>
	(YEAR-MONTH-DAY)</th></tr>";
	
	while($oneRow = mysqli_fetch_assoc($allRows))
	{
			if ($oneRow['album'] != null){
				$stringSoFar = $stringSoFar . $SR . $OH . $oneRow['id'] .  "'>" . htmlspecialchars($oneRow['title']) . $CH . $BC . htmlspecialchars($oneRow['artist']) . $BC . htmlspecialchars($oneRow['album']) . $BC . htmlspecialchars($oneRow['price']) . $BC . htmlspecialchars($oneRow['datepurchased']) . $ER;
			} else {
				$stringSoFar = $stringSoFar . $SR . $OH . $oneRow['id'] . "'>"  . htmlspecialchars($oneRow['title']) . $CH . $BC . htmlspecialchars($oneRow['artist']) . $BC . " " . $BC . htmlspecialchars($oneRow['price']) . $BC . htmlspecialchars($oneRow['datepurchased']) . $ER;
			}		
		
	}
	
	$stringSoFar = $stringSoFar . "</table>";
	
	mysqli_close($conn);
	return $stringSoFar;
}
?>