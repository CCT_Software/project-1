<?php
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file logs the user out.
*/
   session_start();
   require_once('constants.php');
   if(session_destroy()) {
      header("Location: login.php");
	  $_SESSION['logged'] = false;
   }
?>