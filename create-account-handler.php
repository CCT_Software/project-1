<?
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file creates an account.
*/
error_reporting(E_ALL);
//ini_set('display_errors','On');
require_once('db-utils.php');

if(array_key_exists('username', $_POST) && array_key_exists('password', $_POST))
{
	if(usernameExists($_POST['username']))
	{
		echo "<script> alert('Username taken'); </script>";
	}
	else
	{
		addUser($_POST['username'], $_POST['password']);
		
		echo "<script> alert('Account created'); </script>";
	}
}
?>
<script>
window.location = 'login.php';
</script>
