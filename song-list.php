<?/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file shows the songs in the database.
*/?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styling.css">
<?php
		require_once('constants.php');
		echo makeHeader();
	?>
</head>
<body>
	<?php
		error_reporting(E_ALL);
		//ini_set('display_errors','On');
		require_once('db-utils.php');
		echo songsToTable();
	?>
</body>