<?
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file is the handler for purchase.php.
*/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Purchase a song</title>
</head>
<body>
	<?php
		require_once('db-utils.php');
		require_once('purchase.php');
		session_start();
		
		ini_set('display_errors',true); 
		ini_set('display_startup_errors',true); 
		error_reporting (E_ALL|E_STRICT); 
		
		$user = $_SESSION['login_user'];
		$title = $_POST['sName'];
		$artist = $_POST['sArtist'];
		
		$db = connectToDb();
		$sqlQ = "select * from songs where title='$title' and artist='$artist'";
		echo $sqlQ;
		$result = mysqli_query($db, $sqlQ);
		if (!$result) echo "query failed -- lost connection?";
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		  
		$count = mysqli_num_rows($result);
		
		if($count == 1) {
			$oneRow = mysqli_fetch_assoc($result);
			$sAlbum = $row['album'];
			$sPrice = $row['price'];
			$sId = $row['id'];
			$date = date('Y/m/d');
			$sqlI = "INSERT into song_ownership(user, title, artist, album, price, datepurchased, id) VALUES ('$user', '$title', '$artist', '$sAlbum', '$sPrice', '$date', '$sId')";
			header("location: user-page.php");
			$db->query($sqlI);
		}else {
			echo "<script> alert('Something did not go right'); </script>";
		}
		
		
		$db->close();
	?>
</body>
</html>