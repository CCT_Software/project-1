<?php
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file creates the session for the user.
*/
   require_once('db-utils.php');
   $db = connectToDb();
   session_start();
   
   $user_check = mysqli_real_escape_string($_SESSION['login_user']);
   
   $ses_sql = mysqli_query($db,"select username from users where username = '$user_check' ");
   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   
   $login_session = $row['username'];
   
   if(!isset($_SESSION['login_user'])){
      header("location:login.php");
   }
?>