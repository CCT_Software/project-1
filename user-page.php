<?/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file shows the user their account page.
*/?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styling.css">
	<title>User profile</title>
	<?php
		require_once('constants.php');
		require_once('db-utils.php');
		echo makeHeader();
	?>
</head>
<body>
	<?php
		session_start();
		require_once('utils.php');
		$rows = array();
		$user =  $_SESSION['login_user'];
		$userSongs = userSongsToTable($user);
		
		echo "<h1>Account: $user</h1>";
		echo "<p>Purchased</p>";
		echo $userSongs;
	?>
</body>