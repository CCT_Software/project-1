<?php
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file is for util fuctions that involve database connection.
*/
require_once('db-utils.php');
//Most of the code for inserting the file from http://www.php-mysql-tutorial.com/wikis/mysql-tutorials/uploading-files-to-mysql-database.aspx
session_start();
if(array_key_exists('title', $_POST) && array_key_exists('artist', $_POST) && array_key_exists('price', $_POST))
{
	//uploads song to db
	if(isset($_POST['upload']) && $_FILES['userfile']['size'] > 0)
	{
	$fileName = $_FILES['userfile']['name'];
	$tmpName  = $_FILES['userfile']['tmp_name'];
	$fileSize = $_FILES['userfile']['size'];
	$fileType = $_FILES['userfile']['type'];

	$fp      = fopen($tmpName, 'r');
	$content = fread($fp, filesize($tmpName));
	$content = addslashes($content);
	fclose($fp);

	if(!get_magic_quotes_gpc())
	{
		$fileName = addslashes($fileName);
	}

	include 'library/config.php';
	include 'library/opendb.php';
	$conn = connectToDb();

	$query = "INSERT INTO upload (name, size, type, content ) ".
	"VALUES ('$fileName', '$fileSize', '$fileType', '$content')";

	$allRows = mysqli_query($conn, $query);

	//uploads song info to db
	$title = mysqli_real_escape_string($conn, $_POST['title']);
	$artist = mysqli_real_escape_string($conn, $_POST['artist']);
	if(array_key_exists('album', $_POST))
		$album = mysqli_real_escape_string($conn, $_POST['album']);
	else
		$album = 'NULL';
	if(array_key_exists('genre', $_POST))
		$genre = mysqli_real_escape_string($conn, $_POST['genre']);
	else
		$genre = 'NULL';
	$price = mysqli_real_escape_string($conn, $_POST['price']);
	
	if($album === 'NULL' && $genre === 'NULL')
	{
		$qu = "INSERT INTO songs (title, artist, album, genre, price)" .
		"VALUES ('$title', '$artist', $album, $genre, '$price')";
	}
	else if($album === 'NULL')
	{
		$qu = "INSERT INTO songs (title, artist, album, genre, price)" .
		"VALUES ('$title', '$artist', $album, '$genre', '$price')";
	}
	else if($genre === 'NULL')
	{
		$qu = "INSERT INTO songs (title, artist, album, genre, price)" .
		"VALUES ('$title', '$artist', '$album', $genre, '$price')";
	}
	else
	{
		$qu = "INSERT INTO songs (title, artist, album, genre, price)" .
		"VALUES ('$title', '$artist', '$album', '$genre', '$price')";
	}
		
	$allRows = mysqli_query($conn, $qu);
	
	mysqli_close($conn);
	//mysql_query($query) or die('Error, query failed');

	include 'library/closedb.php';

	echo "<br>File $fileName uploaded<br>";
	}
}

HEADER('location: user-page.php');

?>