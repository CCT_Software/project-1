<?
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file allows admins to upload songs.
*/
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styling.css">
	<title>Upload New Song</title>
</head>
<body>
	<?
	require_once('constants.php');
	echo makeHeader();
	//Part of upload code from http://www.php-mysql-tutorial.com/wikis/mysql-tutorials/uploading-files-to-mysql-database.aspx
	?>
	<form method="post" enctype="multipart/form-data" action='upload-handler.php'>
					
		<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/>
		<input name="userfile" type="file" id="userfile" required="required"/>
		<h3><pre>Song Title:	<input type="text" name="title" id="songTitle" required="required"/></pre></h3>
		<h3><pre>Song Artist:	<input type="text" name="artist" id="songArtist" required="required"/></pre></h3>
		<h3><pre>Song Album:	<input type="text" name="album" id="songAlbum"/></pre></h3>
		<h3><pre>Song Genre:	<input type="text" name="genre" id="songGenre" required="required"/></pre></h3>
		<h3><pre>Song Price:	<input type="number" step=".01" name="price" id="songPrice" required="required"/></pre></h3>
		<input name="upload" type="submit" class="box" id="upload" value=" Upload"/>
	</form>
</body>
</html>