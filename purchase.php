<?/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file lets the user purchase a song.
*/?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styling.css">
	<title>Purchase a song</title>
	<?php
		require_once('constants.php');
		echo makeHeader();
	?>
</head>
<body>
	<?php
		require_once('utils.php');
		session_start();

		echo "<form name='purchase' action='purchase-handler.php' method='post'>";
		$title = rawurldecode($_GET['title']);
		$artist = rawurldecode($_GET['artist']);
		echo "<h1>Song: $title</h1>";
		echo "<h5>Artist: $artist</h5>";
		
		if ($_SESSION['logged'] == true){
			echo "<input type='submit' value='Purchase'>";
		}				  
		 
		echo "<input type='hidden' name='sName' value='$title'>";
		echo "<input type='hidden' name='sArtist' value='$artist'>";
		echo "</form>";
	?>
</body>