 <?php
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file is for util fuctions that do not involve 
*/
	 
	error_reporting(E_ALL);
	 //pluralize makes a noun plural
	function pluralize($num, $noun) {
		return $num . " " . $noun . "s\n";
	}

	//hyperlink takes in a $url and returns a link
	//to that $url with the text $linkTxt
	function hyperlink($url, $linkTxt) {
		if ($linkTxt === false) {
			return "<a href='" . $url . "'>" . $url . "</a>\n";
		} else {
			return "<a href='" . $url . "'>" . $linkTxt . "</a>\n";
		}
	}

	//thumbnail takes in a $url and returns an image link
	//of a given $width
	function thumbnail($url, $width) {
		return hyperlink("<img src='" . $url . "' style='width:" . $width . ";'>", true);
	}
	
	//test handles the comparing of the test results to thier desired value
	//since we dont want to be too verbose, we also have a control variable,
	//$printSuccessfulTests, that determines wether or not we print successful
	//tests.
	$printSuccessfulTests = false;
	function test($actVal, $desVal, $normalize = true){
		
		if ($normalize){
			if (is_string($actVal)){
				if (is_string($desVal)){
					$actVal = trim(preg_replace('/\s+/', ' ', $actVal));
					$desVal = trim(preg_replace('/\s+/', ' ', $desVal));
				}
			}
		}
		
		if ($printSuccessfulTests){
			if (strcmp($actVal, $desVal) == 0) {
				echo ".\n";
			} else { 
				echo " FAILURE: Actual value does not match desired.\n";
			}
		} else {
			if (is_string($actVal) && is_string($desVal)){
				if (strcmp($actVal, $desVal) == 0){
					echo "";
				} else {
					echo " FAILURE: Actual value does not match desired.\n";
				}
			} else if (is_array($actVal) && is_array($desVal)){
				if ($actVal == $desVal){
					echo "";
				} else {
					echo " FAILURE:  Actual value does not match desired.\n";
				}
				
			}
		}
	}
	
	/*
	* I believe that as a precondition for this function, there should be a check
	* to make sure that if one $key was a string, then all $key must be a string.
	* If not, tables will render strangely with some rows having more columns than 
	* others, and also affect the formatting. An example of this was included.
	*
	* table_r takes in an array and returns proper html for that table. If a key
	* is a string, it adds a column to that row.
	*/
	function table_r($strArray){
		$htmlStr = "<table border='1'>";
		foreach ($strArray as $key => $curr ){
			
			if (is_string($key)){
				$htmlStr = $htmlStr . "<tr><td>" . $key . "</td><td align='right'>" . $curr . "</td></tr>";
			} else {
				$htmlStr = $htmlStr . "<tr align='right'><td>" . $curr . "</td></tr>";
			}
		}
		$htmlStr = $htmlStr . "</table>";
		return $htmlStr;		
	}
	
	// dropdown : string[], string, boolean or string -> string 
	// Return a dropdown box named by the string, containing options from the passed in the string array.
	// There is an optional parameter that determines the first option in the dropdown box.
	//
	//
	function dropdown($arr, $str, $first = true){
		$htmlStr = "<select name='" . $str . "'>";
		
		if (is_string($first)){
			$htmlStr = $htmlStr . "<option>" . $first . "</option>";
		} else {
			if ($first){
				$htmlStr = $htmlStr . "<option>Select one</option>";
			}
		}
		foreach($arr as $val){
			$htmlStr = $htmlStr . "<option>" . $val . "</option>";
		}
		$htmlStr = $htmlStr . "</select>";
		return $htmlStr;
	}
	
	// radioTable : string, string[], string[] → string
	// Return a table of radio-buttons: 
	// each row is a bank-of-radio-buttons for the corresponding element of $rowNames,
	// and its `name` attribute is $id[$rowName].
	// The allowed values are $colNames.
	//
	//
	function radioTable( $id, $rowNames, $colNames ) {
		$headerRow = radioTableHeader( $colNames );
		$rowsSoFar = "";
		$rowNum = 0;
		foreach ($rowNames as $rowName) {
			$rowsSoFar .= radioTableRow( $rowName, $colNames, $rowNum ) . "\n";
			$rowNum = $rowNum + 1;
		}
		return "<table border='1' name='" . $id . "'>" . $headerRow . $rowsSoFar . "</table>";           
	}
	
	// radioTableHeader : string[] -> string
	// Return a header for a radioTable:
	// It's a single row that contains the header information
	// for a radioTable
	//
	//
	function radioTableHeader($colNames){
		$htmlStr = "<tr><th></th>";
		foreach ($colNames as $colName){
			$htmlStr = $htmlStr . "<th>" . $colName . "</th>";
		}
		$htmlStr = $htmlStr . "</tr>";
		return $htmlStr;
	}
	
	// radioTableRow : string, string[], int -> string
	// Return a single row for a radiotable.
	// 
	//
	function radioTableRow($rowName, $colNames, $rowNum){
		$htmlStr = "<tr><td>" . $rowName . "</td>";
		$colNum = 0;
		foreach ($colNames as $colName){
			$htmlStr = $htmlStr . "<td align='center'><input type='radio' name='resistances[" . $rowNum . "]' value='" . $colNum . "'></input>";
			$colNum = $colNum + 1;
		}
		return $htmlStr;
	}
	
	//creates a table
	function table($id, $rowNames, $colNames){
		$headerRow = tableHeader( $colNames );
		$rowsSoFar = "";
		$rowNum = 0;
		foreach ($rowNames as $rowName) {
			$rowsSoFar .= tableRow( $rowName, $colNames, $rowNum ) . "\n";
			$rowNum = $rowNum + 1;
		}
		return "<table border='1' cellpadding='5' name='" . $id . "'>" . $headerRow . $rowsSoFar . "</table>";
	}
	
	//makes the table header
	function tableHeader($colNames){
		$htmlStr = "<tr>";
		foreach ($colNames as $colName){
			$htmlStr = $htmlStr . "<th>" . $colName . "</th>";
		}
		$htmlStr = $htmlStr . "</tr>";
		return $htmlStr;
	}
	
	//makes a table row
	function tableRow(){
		$htmlStr = "<tr><td>" . $rowName . "</td>";
		$colNum = 0;
		foreach ($colNames as $colName){
			$htmlStr = $htmlStr . "<td align='center'></input>";
			$colNum = $colNum + 1;
		}
		return $htmlStr;
	}
	
	//forms a link link to store info to $_GET
	function linkForGet($details, $linktxt){
		$base = "purchase.php?";
		$paramCount = 1;
		foreach ($details as $prop => $val){
			if ($paramCount > 1){
				$base .= "&" . rawurlencode($prop) . "=" . rawurlencode($val);
			} else {
				$base .= rawurlencode($prop) . "=" . rawurlencode($val);
			}
			$paramCount = $paramCount + 1;
		}
		
		return hyperlink($base, $linktxt);
	}
 ?>