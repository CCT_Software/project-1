<?
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file allows the user to login and create an account.
*/
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styling.css">
	<title>Login/Create account</title>
	<?
	error_reporting(E_ALL);
	//ini_set('display_errors','On');	
		require_once('constants.php');
		require_once('db-utils.php');
		echo makeHeader();
		$db = connectToDb();
		
		//Much of the code below taken from: https://www.tutorialspoint.com/php/php_mysql_login.htm
		session_start();
		
		if($_SERVER["REQUEST_METHOD"] == "POST") {
		// username and password sent from form 
		  $myusername = mysqli_real_escape_string($db,$_POST['username']);
		  $mypassword = mysqli_real_escape_string($db,$_POST['password']); 
		  
		  $sql = "SELECT username FROM users WHERE username = '$myusername' and password = '$mypassword'";
		  $result = mysqli_query($db,$sql);
		  $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		  $active = $row['active'];
		  
		  $count = mysqli_num_rows($result);
		  mysqli_close($db);
		  
		  // If result matched $myusername and $mypassword, table row must be 1 row
			
		  if($count == 1) {
			 session_register("myusername");
			 $_SESSION['login_user'] = $myusername;
			 $_SESSION['logged'] = true;
			 
			 header("location: user-page.php");
		  }else if ($count == 0) {
			 echo "<script> alert('Incorrect login credentials'); </script>";
		  }
		  
		  $db->close();
	   }
	?>
	<script>
		function validate(){
			return true;
		}
	</script>
</head>
<body>
	<?php
		require_once('utils.php');
		require_once('db-utils.php');
	
	?>
		<form action='' name='login' method='post' onsubmit='return validate();'>
			<pre><h1>Login:</h1></pre>
			<pre>Username:<input type='text' name='username' id='username'></pre>
			<pre>Password:<input type='password' name='password'></pre>
			<input type='submit' value='Login'/>
		</form>
		</br></br>
		<form action='create-account-handler.php' method='post' name='create-account'>
			<pre><h1>Make account:<h1></pre>
			<pre>Username:<input type='text' name='username'></pre>
			<pre>Password:<input type='password' name='password'></pre>
			<input type='submit' value='create'>
		</form>
</body>