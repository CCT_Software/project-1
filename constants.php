<?php
/**
* Collier Crisanti & Travis Guyer
* ITEC 325 Project
* This file has constants that multiple webpages use.
*/
		session_start();

		define('standardCols', serialize(array("Song", "Artist", "Album", "Price")));
		
		function makeHeader(){
			if ($_SESSION['logged'] == false || $_SESSION['logged'] == null){
				return "<a href='song-list.php'><img src='logo-small.png'/></a>
					</br><table border='1'>
					<tr>
					<td><a href='song-list.php'><div>Store</div></a></td>
					<td><a href='login.php'><div>Login/Create Account</div></a></td>
					</tr>
					</table>";
			} else {
				$user = htmlspecialchars($_SESSION['login_user']);
				return "<a href='song-list.php'><img src='logo-small.png'/></a>
					</br><table border='1'>
					<tr>
					<td><a href='song-list.php'><div>Store</div></a></td>
					<td><a href='user-page.php'><div>Account</div></a></td>
					<td><a href='logout.php'><div>Log Out</div></a></td>
					</tr>
					</table><pre> --- $user ---</pre>";
			}
		}
		

?>